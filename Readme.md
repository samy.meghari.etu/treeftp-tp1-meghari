## PROJET TREE FTP - MEGHARI Samy

## RESUMÉ
Le but de ce TP est la mise en oeuvre d'un parcours en profondeur qui permet d'afficher en sorti l'arborescence d'un répertoire distant accessible via un protocole FTP

## JAVADOC
>> mvn javadoc:javadoc

## JAR
>> mvn package

## EXEMPLE D'EXECUTION

 java -jar target\TREE.jar [IP SERVEURR]

 java -jar target\TREE.jar [IP SERVEURR] [PROFONDEUR]

 java -jar target\TREE.jar [IP SERVEURR] [IDENTIFANT ] [MOT DE PASSE]

 java -jar target\TREE.jar [IP SERVEURR] [IDENTIFANT ] [MOT DE PASSE] [PROFONDEUR]


## ARCHITECTURE

 **ClientFTP** s'occupe de la connection du client

 **Tree** s'occupe du parcours en profondeur

 **ErrorException** est levé dans la plus part des erreurs que j'ai pu identifier

 **Main** lance l'execution

## EXTRAITS DE CODE

Methode qui permet au client de se connecter 
```java
	public void init(String ip, int port)throws IOException, ErrorException {

		mysocket = new Socket(ip, port);
        OutputStream out = mysocket.getOutputStream();
        PrintWriter printer = new PrintWriter(out, true);

		InputStream in = mysocket.getInputStream();
		InputStreamReader isr = new InputStreamReader(in);
		BufferedReader reader = new BufferedReader(isr);

		 String content = reader.readLine();
	        System.out.println(content);

		printer.println("USER " + getUser().trim() + "\r\n");
		content = reader.readLine();
		System.out.println(content);

		printer.println("PASS " + getPassword().trim() + "\r\n");
		content = reader.readLine();
		System.out.println(content);

		}
```

Classe gestion d'erreurs
```java
public class ErrorException extends Exception{

	private static final long serialVersionUID = 1L;
	public ErrorException() {
		super("failed to connect");
	}
	public ErrorException(String msg) {
		super(msg);
		
	}
}
```

Methode pour le parcours en profondeur
```java
		public void displayFolders(String dir, int profondeur)throws IOException, ErrorException {
			List<String> myFolder = new ArrayList<>();
				myFolder = displaymyFolder(dir);
			
			if (dir == "") {
				System.out.println("/");
			} else {
				System.out.println(dir);
			}
			if (myFolder.isEmpty()) {
				System.out.println("empty");
				return;
			}
			for (String s : myFolder) {
				switch (s.charAt(0)) {
				case ('d'):
					System.out.println("    └── " + s);
					walker(dir + "/" + analyse(s), "", profondeur - 1);
					break;
				case ('l'):
					System.out.println("    └──" + s);
					break;

				case ('-'):
					System.out.println("   └── " + s);
					break;

				default:
					break;
				}
			}
			}
```

Methode de gestion des inputs utilisateurs
```java
	public static void main(String[] args) throws IOException, ErrorException {
		int len = args.length;
		ClientFTP client;
		String serveur = "";
		Tree tree;
		if (len >= 1 && len <= 4) {
			serveur = args[0];
			client = new ClientFTP("anonymous", "anonymous");
			client.init(serveur, 21);
			tree = new Tree(client);

			if (len == 1) {

				tree.displayFolders("", 3);
			} else if (len == 2) {

				tree.displayFolders("", Integer.parseInt(args[1]));
			} else if (len == 3) {

				tree.displayFolders("", 3);
			} else if (len == 4) {

				tree.displayFolders("", Integer.parseInt(args[3]));
			}
		}

		else {
			System.out.println("please check the readme");
		}

	}
```
