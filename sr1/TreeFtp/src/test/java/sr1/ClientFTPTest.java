package sr1;

import java.io.IOException;
import java.net.UnknownHostException;

import org.junit.Before;
import org.junit.Test;

public class ClientFTPTest {
	private ClientFTP client;
	
	@Before
	public void setupBefore(){
		client = new ClientFTP("test", "test");
	}
	
	@Test(expected = UnknownHostException.class)
	public void testConnectWhenfalseAdress() throws UnknownHostException, IOException,ErrorException {
		
		client.init("ftp.randomadress.com", 75);
	}
}
