package sr1.main;
import java.io.IOException;

import sr1.*;
/**
 * @author Samy MEGHARI
 * 
 * */

public class Main {

	/**
	 *lancera Tree  en fonction des arguments passés
	 *
	 *@param args les arguments passées
	 *@throws IOException
	 *@throws ErrorException
	 */
	public static void main(String[] args) throws IOException, ErrorException {
		int len = args.length;
		ClientFTP client;
		String serveur = "";
		Tree tree;
		if(len>= 1 && len<=4) {
			serveur = args[0];
			client = new ClientFTP("anonymous","anonymous");
			client.init(serveur, 21);
			tree = new Tree(client);
		
		if(len ==  1) {

			tree.displayFolders("", 3);
		}
		else if(len == 2) {

			tree.displayFolders("", Integer.parseInt(args[1]));
		}
		else if(len == 3) {

			tree.displayFolders("", 3);
		}
		else if(len == 4) {

			tree.displayFolders("", Integer.parseInt(args[3]));
		}
		}
	
		else  {
			System.out.println("please check the readme");
		}



	}
}
