package sr1;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
/**
 * @author Samy MEGHARI
 * 
 * */
public class Tree {
	private String myfile;
	private ClientFTP myclient;
	
	public Tree(ClientFTP client) {
		myclient = client;}
	

	/**
	 * Parse le retour de la commande LIST
	 */
	private String analyse(String myreponse) {
		String reponse = myreponse.replaceAll("(\\s)+", " ");
		String[] tab = reponse.split(" ");
		if (tab.length > 9) {
			for (int i = 8; i < tab.length; i++) {
				this.myfile += " " + tab[i];
			}
		} else {
			this.myfile = tab[8];
		}
		return myfile;

	}
	
	/**
	 * affiche l'arborescence d'un repertoir
	 * 
	 * @param dir  le repertoire a parcouru
	 * @param res espace pour distinguer l'arborescence
	 * @param profondeur profondeur maximale
	 * @throws IOException
	 * @throws @throws ErrorException
	 */
	private void myExplorer(String dir, String res, int profondeur)
			throws IOException, ErrorException {
		if (profondeur > 0) {
			List<String> myFolder = new ArrayList<>();
				myFolder = displaymyFolder(dir);

			res += "    ";

			for (String S : myFolder) {
				
				switch (S.charAt(0)) {
				case ('d'):
					System.out.println(res + "    └── " + analyse(S));
					myExplorer(dir + "/" + analyse(S), res, profondeur - 1);

				case ('l'):
					System.out.println(res + "    └──" + analyse(S));

				case ('-'):
					System.out.println(res + "    └── " + analyse(S));

				default:
					break;
				}
			}
		} else {
			return;
		}
	}
	
	/**
	 * Affiche l'arborescence des dossiers
	 * 
	 * @param dir  le repertoire parcouru
	 * @param profondeur profondeur maximale
	 * @throws IOException
	 * @throws ErrorException
	 */
		public void displayFolders(String dir, int profondeur)throws IOException, ErrorException {
			List<String> myFolder = new ArrayList<>();
				myFolder = displaymyFolder(dir);
			
			if (dir == "") {
				System.out.println("/");
			} else {
				System.out.println(dir);
			}
			if (myFolder.isEmpty()) {
				System.out.println("empty");
				return;
			}
			for (String s : myFolder) {
				switch (s.charAt(0)) {
				case ('d'):
					System.out.println("    └── " + s);
					myExplorer(dir + "/" + analyse(s), "", profondeur - 1);
					break;
				case ('l'):
					System.out.println("    └──" + s);
					break;

				case ('-'):
					System.out.println("   └── " + s);
					break;

				default:
					break;
				}
			}
			}
		/**
		 * Recupere la sortie de LIST
		 * 
		 * @param ip , Ip du nouveau socket
		 * @param port, Port du nouveau socket
		 * @return transforme en liste la sortie de LIST
		 */
		private List<String> getData(String ip, int port) throws UnknownHostException, IOException {
			OutputStream out = this.myclient.getSocket().getOutputStream();
	        PrintWriter printer = new PrintWriter(out, true);
	        printer.println("LIST " + "\r\n");

			Socket mySocket;
			List<String> myFolder = new ArrayList<>();
			mySocket = new Socket(ip, port);
			InputStream in = mySocket.getInputStream();
			InputStreamReader isr = new InputStreamReader(in);
			BufferedReader reader2 = new BufferedReader(isr);
			String dir;
			while ((dir = reader2.readLine()) != null) {
					myFolder.add(dir);
				}

			mySocket.close();
			return myFolder;
		}
		
		/**
		 *Renvoie le contenu d'un dossier
		 * 
		 * @param myDir le dossier parcouru
		 * @return la liste des elements du repertoire
		 * @throws IOException

		 */
		public List<String> displaymyFolder(String myDir) throws IOException {
			List<String> myFolder = new ArrayList<>();
	        OutputStream out = this.myclient.getSocket().getOutputStream();
	        PrintWriter printer = new PrintWriter(out, true);

	        InputStream in = this.myclient.getSocket().getInputStream();
	        InputStreamReader isr= new InputStreamReader(in);
	        BufferedReader reader = new BufferedReader(isr);
	        
				printer.println("CWD "+myDir+"\r\n");
				reader.readLine();
				printer.println("PWD "+"\r\n");
				reader.readLine();
				printer.println("PASV "+"\r\n");
				String res = "";
				int newport = 0;
				String newip = null;
				res= reader.readLine();
				
				if(res.toLowerCase().startsWith("227 entering passive mode")) {
					String temp = res.substring(res.indexOf("(")+1,res.indexOf(")"));
					newip = this.myclient.getIP(temp);
					newport = this.myclient.getPort(temp);
					myFolder = getData(newip, newport);
				}
				res= reader.readLine();

			return myFolder;
		}
}
