package sr1;

import java.io.*;
import java.net.*;

/**
 * @author Samy MEGHARI
 * 
 * */


public class ClientFTP {
	private String user;
	private String password;
	private Socket mysocket;
	public ClientFTP(String user, String password) {
		this.user=user;
		this.password= password;
	}
	
	public String getUser() {
		return user;
	}
	public Socket getSocket() {
		return mysocket;
	}
	public String getPassword() {
		return password;
	}

	/**
	 * Lance le client
	 * 
	 * @param ip , l'ip du serveur
	 * @param port   , le port du serveur
	 * @throws IOException
	 * @throws ErrorException
	 */
	public void init(String ip, int port)throws IOException, ErrorException {

		mysocket = new Socket(ip, port);
        OutputStream out = mysocket.getOutputStream();
        PrintWriter printer = new PrintWriter(out, true);

		InputStream in = mysocket.getInputStream();
		InputStreamReader isr = new InputStreamReader(in);
		BufferedReader reader = new BufferedReader(isr);

		 String content = reader.readLine();
	        System.out.println(content);

		printer.println("USER " + getUser().trim() + "\r\n");
		content = reader.readLine();
		System.out.println(content);

		printer.println("PASS " + getPassword().trim() + "\r\n");
		content = reader.readLine();
		System.out.println(content);

		}
	/**
	 * Parse le retour de la commande PASV et recupere l'IP
	 * @param ip commande FTP PASV nettoye
	 * @return  IP reconstruite
	 */
	public String getIP(String ip) {
		String[] s = ip.split(",");
		return s[0]+ "." + s[1]+ "." + s[2]+ "." + s[3];
	}
	
	/**
	 * Parse le retour de la commande PASV , recupere le port et le recalcul
	 * 
	 * @param port retour de la commande PASV nettoye
	 * @return le port recalculer
	 */
	public int getPort(String port) {
		String[] s = port.split(",");
		int i = Integer.parseInt(s[4])*256 + Integer.parseInt(s[5]);
		return i;
	}

}
